<?php
//Primero crearemos una clase que se llame clima 
class Clima
{
//Caragteristicas publicas de cada lugar 
public $id;
public $ciudad;
public $temp_maxima;
public $temp_minima;
public $sensacion_term;
public $viento;
public $visibilidad;
public $humedad;
public $punt_recio;
//constructor
public function __construct(string $ide, string $ciud,string $temp_max, string $temp_min, string $sens, string $vient, string $vis, string $hum, string $punt)
{
$this->id = $ide;
$this->ciudad = $ciud;
$this->temp_maxima = $temp_max;
$this->temp_minima = $temp_min;
$this->sensacion_term = $sens;
$this->viento = $vient;
$this->visibilidad = $vis;
$this->humedad = $hum;
$this->punt_recio = $punt;
}
}
?>