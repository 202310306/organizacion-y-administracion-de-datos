<?php
//requerimos llamar al archivo que tiene la clase
require_once('./ClassClima.php');
//definir el normbre para el archivo json
$archivo_json = "datos.json";
//instanciamos la clase para el numero de objetos que usaremos
$datos1 = new Clima("1","Lerdo","35","18","35","13","16","14%","7");
$datos2 = new Clima("2","Chihuaha","35","11","14","7","16","35%","-2");
$datos3 = new Clima("3","Saltillo","33","28","20","10","16","41%","8");
$datos4 = new Clima("4","CDMX","31","25","16","9","9.7","63%","9");
$datos5 = new Clima("5","Durango","35","29","14","11","16","34%","2");
//creamos un arreglo para los datos
$datos = array();
//agregamos los 5 datos al arreglo
$datos[] = $datos1;
$datos[] = $datos2;
$datos[] = $datos3;
$datos[] = $datos4;
$datos[] = $datos5;
//creamos el archivo json a partir de los objetos que instanciamos
$json_string = json_encode($datos);
//escribiremos en un archivo común un archivo en modo 'w'
$arch = fopen($archivo_json,'w');
if( $arch == false ) {
echo ( "Error al abrir el archivo" );
exit();
}
//escribimos en el archivo el string que contiene el json
fwrite($arch,$json_string);
//cerramos el archivo
fclose($arch);
//cerramo y redirigimos para mostrar datos en pantalla desde el archivo creado
echo '<h3>Datos escritos en agenda.json </h3>';
header("refresh:2;url=mostrar_json.php");
?>