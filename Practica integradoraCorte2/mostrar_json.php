<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="./css/bootstrap.min.css">
        <link rel="stylesheet" href="./css/estilos.css">
        <title>Datos de Clima</title>
    </head>
    <body>
        <?php
        //El archivo con los datos es datos.json
        $archivo = 'datos.json';
        //verificamos que existe y tratamos de abrirlo
        if (file_exists("$archivo")) {
            $x = fopen($archivo, 'r')
            or die("Error: No se puede abrir el archivo json");
            //verificamos tamaño de archivo
            $size = filesize($archivo);
            //leemos el archivo y recuperamos todo el contenido
            $contenido = fread($x, $size);
            //cerramos arhivo
            fclose($x);
            //pasamos a realizar un arreglo para los elementos de la lista de datos
            $datosclima = json_decode($contenido, true);
            $numdatos = count($datosclima);?> 
            <div class="container">
                <h1 class="titulo">Lista de datos Climaticos</h1>
                <!-- diseño de la tabla-->
                <div class="table-responsive">
                    <table class="table">
                        <thead class="table table-sm table-warning">
                            <!-- diseño de encabezado-->
                            <tr>
                                <th>ID</th>
                                <th>Ciudad</th>
                                <th>Temp Max</th>
                                <th>Temp Min</th>
                                <th>Sensacion Termica</th>
                                <th>Viento Km/h</th>
                                <th>Visibilidad Km</th>
                                <th>Humedad %</th>
                                <th>Punto de rocio</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            for ($i = 0; $i < $numdatos; $i++) {
                                //Matriz de los elemetos y diseño de las columnas
                                echo '<tr>';
                                echo '<td class="table-warning">'  . $datosclima[$i]['id'] . '</td>';
                                echo '<td class="table-info">' . $datosclima[$i]['ciudad'] . '</td>';
                                echo '<td class="table-info">' . $datosclima[$i]['temp_maxima'] . '</td>';
                                echo '<td class="table-info">' . $datosclima[$i]['temp_minima'] . '</td>';
                                echo '<td class="table-info">' . $datosclima[$i]['sensacion_term'] . '</td>';
                                echo '<td class="table-info">' . $datosclima[$i]['viento'] . '</td>';
                                echo '<td class="table-info">' . $datosclima[$i]['visibilidad'] . '</td>';
                                echo '<td class="table-info">' . $datosclima[$i]['humedad'] . '</td>';
                                echo '<td class="table-info">' . $datosclima[$i]['punt_recio'] . '</td>';
                                echo '</tr>';
                            } 
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <?php
                } 
                ?>
                </body>
                </html>